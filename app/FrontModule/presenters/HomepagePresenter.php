<?php

namespace FrontModule;

use Nette;
use App\Model;
use Nette\Application\UI;
use Nette\Forms\Controls;
use Nette\Application\UI\Form;
use Nette\Utils\Image;
use Nette\Utils\Strings;
use Nette\Utils\Finder;
use Nette\Utils\Paginator;
use Nette\Mail\Message;
use Nette\Mail\SendmailMailer;

class HomepagePresenter extends BasePresenter
{

    private $potomciKategorii = array();
    private $paginator;

    public function actionDefault($id, $page = 1)
    {
        $paginator = new Nette\Utils\Paginator;
        $paginator->setItemCount($this->product->getPostsCount($this->getView()));
        $paginator->setItemsPerPage(6);
        $paginator->setPage($page);
        $length = $paginator->getLength();

        if ($page < 1 || $page > $length) {
            //$paginator->setPage(1);
        }
        $this->paginator = $paginator;
        $this->template->paginator = $this->paginator;
        $this->template->absImagePath = $this->context->parameters['wwwDir'];
        $this->paginator->setItemCount($this->product->findBy(array('active' => 1))->count());
        $categories = $this->category->findBy(array('active' => 1));
        $this->template->categories = $categories;
        foreach ($categories as $key => $item) {
            $newArray[$key] = array('id' => $item->id,'name' =>$item->name,'parent_id' => $item->parent_id) ;
        }
  
        // produkty do šablony

        $products = $this->product->findBy(array('active' => 1))->limit($this->paginator->getLength(), $this->paginator->getOffset());
        $this->template->products = $products->order('name ASC');
        if ($id) {
            $this->handleShowCategory($id);
        }
    }

    public function renderDefault()
    {
        $this->template->paginator = $this->paginator;
        
    }

    public function actionShow($id)
    {
        $this->template->formSendDone = false;
        $productAttributes = $this->productAttribute->findBy(array('product_id' => $id, 'active' => 1));
        $this->template->attributes = $productAttributes;
    }

    public function renderShow($id)
    {
        $product = $this->product->findBy(array('id' => $id, 'active' => 1))->fetch();
        if ($product) {
            $this->template->product = $product;
        } else {
            $this->flashMessage("NOT EXISTS.", "warning");
            $this->redrawControl('flashMessages');
            $this->redirect('default');
        }

        $categories = $this->category->findBy(array('active' => 1));
        $this->template->categories = $categories;
    }

    public function renderSearch($name, $page = 1)
    {
        $products = $this->product->findBy(array('name LIKE ?' => "%$name%", 'active' => 1));
        $this->template->products = $products->order('name ASC');

        $categories = $this->category->findBy(array('active' => 1));
        $this->template->categories = $categories;
    }

    public function handleSearch($name)
    {
        if (!$this->isAjax()) {
            $this->redirect("this");
        }

        $this->flashMessage("SUCCESS.$name", "success");
        $this->redrawControl('flashMessages');
        $this->redirect('search', array($name));
    }

    public function getItemsForChilds($id, $parent_id = 0, $level =0)
    {
        $categories = $this->category->findBy(array('active' => 1, 'parent_id' => $id));
        return $categories;
    }


/*
    public function maPotomky($id, $parent_id = 0, $level =0)
    {
        $deti = array();

        $deti = $this->category->findBy(array('parent_id' => $id));
        $this->template->maPotomky = true;
        return $deti;
    }
*/
    public function maPotomkyPotomku($id, $parent_id = 0, $level =0)
    {
        $childs = array($id);
        $childs = $this->category->findBy(array('parent_id' => $id));
        foreach ($childs as $key => $child) {
            if ($child->parent_id == $id) {
                $this->potomciKategorii[] = $child->id;
                $this->maPotomkyPotomku($child->id);
            }
        }
        //print_r($potomci); 
        return $this->potomciKategorii;
    }


    public function handleShowCategory($id)
    {
        if (!is_numeric($id)) {
            $this->flashMessage("Nehrabej se, prosím, v URL.", "danger");
            return;
        }
        $childs = array();
        $childs = $this->maPotomkyPotomku($id);
        $childs2 = array_unshift($this->potomciKategorii, $id);

        $kate = array(1,2);
        $test = $this->potomciKategorii;

        //get products count for paginator
        $productsCount = $this->product->findBy(array('category'=>$test, 'active' => 1));
        $this->paginator->setItemCount($productsCount->count());

        $products = $this->product->findBy(array('category'=>$test, 'active' => 1))
            ->limit($this->paginator->getLength(), $this->paginator->getOffset());

        $this->template->products = $products->order('name ASC');

        $this->redrawControl('productsList');
        $this->redrawControl('flashMessages');
    }


    protected function createComponentSearchForm()
    {
        $form = new UI\Form;
        $form->addText('search', 'Product:');
        $form->addSubmit('find', 'GO');
        $form->onSuccess[] = $this->searchFormSubmit;
        return $form;
    }

    // volá se po úspěšném odeslání formuláře
    public function searchFormSubmit(UI\Form $form, $values)
    {
        $values = $form->getValues();
        $this->redirect(':Front:Homepage:search', array($values->search));
    }

    protected function createComponentContactForm()
    {
        $productId = $this->getParameter('id');
        $product = $this->product->findBy(array('id' => $productId))->fetch();
        $productName = $product->name;
        $form = new UI\Form;
        $form->addText('name', 'Name:')->addRule(Form::MIN_LENGTH, 'Name must contain atleast %d words', 3);
        //$form->addText('email', 'Product:')->addRule(Form::EMAIL, 'E-mail má nesprávný tvar')->setEmptyValue('@')
        //    ->addRule(Form::FILLED, 'Vyplňte váš e-mail');

        $form->addText('email', 'E-mail:')->addRule(Form::EMAIL)->addRule(Form::FILLED);

        $form->addText('phone', 'Phone:')->addRule(Form::MAX_LENGTH, 'Name must contain atleast %d words', 15);

        $form->addTextArea('body', 'Body:')->addRule(Form::MIN_LENGTH, 'Text must contain atleast %d words', 3);

        // antispam
        $c1 = date('j')+3;
        $c2 = date('N')+2;
        $s = $c1 + $c2;
        $form->addText('soucet', sprintf('%s+%s = ?', $c1, $c2))
            ->setRequired('Bad result')
            ->addRule(Form::EQUAL, 'CAPTCHA', $s);
        $form->addHidden('c1', $c1);
        $form->addHidden('c2', $c2);
        $form->addHidden('productName', $productName);
        $form->addSubmit('send', 'Send');

        $form->onSuccess[] = $this->contactFormSubmit;
        $form->onValidate[] = $this->contactFormValidate;
        return $form;
    }

    public function contactFormValidate(UI\Form $form, $values)
    {
        $this->redrawControl('contactFormSnippet');
    }

    // volá se po úspěšném odeslání formuláře
    public function contactFormSubmit(UI\Form $form, $values)
    {
        if ($form->isSuccess()) {
            $id = $this->getParameter('id');
            $values = $form->getValues();
      
            $mail = new Message;
            $mail->setFrom($values->email)
                 ->addTo($this->parameters['emailTo'])
                 ->setSubject('Dotaz o info k produktu :'.$values->productName);


            $template = $this->createTemplate();
            $template->setFile(__DIR__ . '/../templates/email/emailTemplate.latte');
            $template->title = 'Dotaz o info k produktu';
            $template->values = $values;
            
            $mail->setHtmlBody($template);

            $mailer = new SendmailMailer;
            $mailer->send($mail);

            $query = $this->productMessage->addItem($values->name, $values->email, $values->phone, $values->body, $id);
    
            $this->template->formSendDone = true;
            $form->setValues(array(), true);

            $this->redrawControl('contactFormSnippet');
            $this->redrawControl('flashMessages');
        }
    }

    /**
     * zmena stranky
     * @param  int $page page from paginator
     * @return int page
     */
    public function handleChange($page)
    {
        if (!$this->isAjax()) {
            $this->redirect("this");
        }
        $this->paginator->setPage($page);
        $this->redrawControl('productsList');
        return $this;
    }
}
