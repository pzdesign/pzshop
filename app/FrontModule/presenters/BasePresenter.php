<?php

namespace FrontModule;

use Nette;
use Nette\Utils\Paginator;
use App\Model\UserManager;
use App\Model\RulesForms;
/**
 * Base presenter for all application presenters.
 */
abstract class BasePresenter extends \AdminModule\BasePresenter
{

    public function beforeRender()
    {
        $this->setLayout('layout');
        $this->template->parameters = $this->parameters;
    }

    public function handleLogOut()
    {
        $this->user->logout();
        $this->flashMessage('Byl jste odhlášen', 'info');
        $this->redirect(':Front:Homepage:default');
    }
}
