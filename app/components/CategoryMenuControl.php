<?php 
namespace App\Components;

use Nette\Application\UI\Control;
use App\Model\CategoryRepository;
use App\Model\ProductRepository;

class CategoryMenuControl extends Control
{

    public function __construct(CategoryRepository $category,ProductRepository $product) {
        $this->category = $category;
        $this->product = $product;
    }

	public function render()
	{
		$template = $this->template;
		$allCategories = $this->category->getAllPosts();
        $this->template->categories = $allCategories;
		$template->render(__DIR__ . '/CategoryMenuControl.latte');
	}




}