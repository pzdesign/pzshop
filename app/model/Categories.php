<?php

namespace App\Model;

class Categories extends TableManager {

    /**
     * Name of the database table
     * @var string
     */
    protected $tableName = 'pzshop_categories';
    private $tree = '';
    private $treeIds = array();

    /** @return Nette\Database\Table\ActiveRow */
    public function getCategoriesTree() {

        $result = $this->database->table('pzshop_categories')->where('parent_id IS NULL OR parent_id = ?', 0)->order('name ASC');

        #for avoiding some errors
        if (count($result) > 0) {
            #start the list
            $this->tree .= '<ul>';
            foreach ($result as $row) {
                #print the item, you can also make links out of these
                $this->tree .= '<li id="' . $row['id'] . '">' . $row['name'];
                #recursive function(made in next step) for getting all the subs by passing id of main item
                $this->getChildren($row['id']);
                $this->tree .= '</li>';
            }
            #end the list
            $this->tree .= '</ul>';
        }
#some message if the database is empty
        else {
            $this->tree .= 'No Items';
        }
    }

    public function getChildren($parentId, $level = 1) {
        $result = $this->database->table('pzshop_categories')->where('parent_id = ?', $parentId)->order('name ASC');
        #for avoiding some errors
        if (count($result) > 0) {
            #start the list
            $this->tree .= '<ul>';
            foreach ($result as $row) {
                #print the item, you can also make links out of these
                $this->tree .= '<li id="' . $row['id'] . '">' . $row['name'];
                #this is similar to our last code
                #this function calls it self, so its recursive
                $this->getChildren($row['id'], $level + 1);
                $this->tree .= '</li>';
            }
            #close the list
            $this->tree .= '</ul>';
        }
    }

    public function getCategoriesTreeIds($id) {

        $category = $this->database->table('pzshop_categories')->get($id);
        array_push($this->treeIds, array('id' => $category['id'], 'name' => $category['name']));

        $result = $this->database->table('pzshop_categories')->where('parent_id = ?', $id);

        if (count($result) > 0) {
            foreach ($result as $row) {
                array_push($this->treeIds, array('id' => $row['id'], 'name' => $row['name']));
                $this->getChildrenIds($row['id']);
            }
        }

        return $this->treeIds;
    }

    public function getChildrenIds($parentId, $level = 1) {
        $result = $this->database->table('pzshop_categories')->where('parent_id = ?', $parentId)->order('name ASC');
        if (count($result) > 0) {
            foreach ($result as $row) {
                array_push($this->treeIds, array('id' => $row['id'], 'name' => $row['name']));
                $this->getChildrenIds($row['id'], $level + 1);
            }
        }
    }

    public function deleteCategoryTreeBranch($id) {
        $result = $this->database->table('pzshop_categories')->get($id);
        $this->deleteChildren($id);
        $this->deleteCategoriesHasPublication($id);
        if ($result) {
            $result->delete();
        }
    }

    public function deleteChildren($parentId, $level = 1) {
        $result = $this->database->table('pzshop_categories')->where('parent_id = ?', $parentId);
        if (count($result) > 0) {
            foreach ($result as $row) {
                $this->deleteChildren($row['id'], $level + 1);
                $this->deleteCategoriesHasPublication($row['id']);
                $row->delete();
            }
        }
    }


    public function getParentCategories() {
        return $this->database->table('categories')->where('parent_id IS NULL OR parent_id = ?', 0)->order('name ASC')->fetchPairs('id', 'name');
    }

}
