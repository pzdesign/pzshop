<?php 
namespace App\Model;

class CategoryRepository extends TableManager
{

    private $connection;
    protected $tableName = 'pzshop_categories';


    private $tree = '';
    private $treeIds = array();

    public function getPostsLimited($limit, $offset)
    {
        return $this->findAll()->limit($limit, $offset);
    }

    public function getAllPosts()
    {
        return $this->findAll();
    }

    public function getByName($name)
    {
        return $this->findAll()->fetchPairs($name);
    }


    public function getPostsCount()
    {
        return $this->findAll()->count();
    }

    public function getVisiblePostsCount()
    {
        return $this->findAllVisible()->count();
    }

    public function getById($id)
    {
        return $this->findBy(array('id' => $id));
    }

    public function findByName($name)
    {
        return $this->findBy(array('name' => $name));
    }

    public function findByParent($parentId)
    {
        return $this->findBy(array('parent_id' => $parentId));
    }

    public function addItem($name, $parent_id, $active)
    {
        $query = $this->findAll()->insert(array('name' => $name, 'parent_id' => $parent_id, 'active' => $active));
    }


    public function editPost($id, $name, $parent_id, $active)
    {
        $this->findBy(array('id' => $id))->update(array('name' => $name, 'parent_id' => $parent_id, 'active' => $active));
    }


    public function setActive($id, $active)
    {
        $this->findBy(array("id" => $id))->update(array("active" => $active));
    }


    public function removeItem($id)
    {
        $this->findBy(array('id' => $id))->delete();
    }
/*
    public function buildTree($arrs, $id, $parent_id=0, $level=0) {
        foreach ($arrs as $arr) {          
            if ($arr->parent_id == $parent_id) {
                $this->buildTree($arrs, $arr['id'], $level+1);
            }    
        }
    }
*/

    public function getParents($id)
    {
        $potomci = $this->findBy(array('parent_id' => $id));
        return $potomci;
    }
    public function getParentsCount($id)
    {
        $potomciCount = $this->findBy(array('parent_id' => $id))->count();
        return $potomciCount;
    }
}
