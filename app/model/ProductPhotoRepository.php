<?php 
namespace App\Model;

class ProductPhotoRepository extends TableManager
{

    private $connection;
    protected $tableName = 'pzshop_product_messages';

    public function getAllPosts()
    {
        return $this->findAll();
    }

    public function addItem($product_id, $src, $active)
    {
        $query = $this->findAll()->insert(array(
        'product_id' => $product_id,
        'src' => $src,
        'active' => $active));
    }

    public function removeItem($id)
    {
        $this->findBy(array('id' => $id))->delete();
    }
}
