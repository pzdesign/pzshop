<?php 
namespace App\Model;

class ProductMessageRepository extends TableManager
{

    private $connection;
    protected $tableName = 'pzshop_product_messages';

    public function getPostsLimited($limit, $offset)
    {
        return $this->findAll()->limit($limit, $offset);
    }

    public function getAllPosts()
    {
        return $this->findAll();
    }

    public function getByName($name)
    {
        return $this->findAll()->fetchPairs($name);
    }


    public function getPostsCount()
    {
        return $this->findAll()->count();
    }

    public function getVisiblePostsCount()
    {
        return $this->findAllVisible()->count();
    }

    public function getById($id)
    {
        return $this->findBy(array('id' => $id));
    }

    public function addItem($name, $email, $phone, $body, $product_id, $confirmed = 0)
    {
        $query = $this->findAll()->insert(array(
        'name' => $name,
        'email' => $email,
        'phone' => $phone,
        'body' => $body,
        'product_id' => $product_id,
        'confirmed' => $confirmed));
    }

    public function setActive($id, $active)
    {
        $this->findBy(array("id" => $id))->update(array("confirmed" => $active));
    }


    public function removeItem($id)
    {
        $this->findBy(array('id' => $id))->delete();
    }
}
