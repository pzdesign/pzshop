<?php 
namespace App\Model;

class ProductAttributeRepository extends TableManager
{

    private $connection;
    protected $tableName = 'pzshop_product_attributes';

    public function getPostsLimited($limit, $offset)
    {
        return $this->findAll()->limit($limit, $offset);
    }

    public function getAllPosts()
    {
        return $this->findAll();
    }

    public function getByName($name)
    {
        return $this->findAll()->fetchPairs($name);
    }


    public function getPostsCount()
    {
        return $this->findAll()->count();
    }

    public function getVisiblePostsCount()
    {
        return $this->findAllVisible()->count();
    }

    public function getById($id)
    {
        return $this->findBy(array('id' => $id));
    }

    public function searchByName($name)
    {
        return $this->findBy(array($name));
    }

    public function getByCategory($id)
    {
        return $this->findBy(array('category' => $id));
    }




    public function addItem($name, $productId, $text, $active)
    {
        $query = $this->findAll()->insert(array('name' => $name, 'product_id' => $productId, 'text' => $text, 'active' => $active));
    }


    public function editPost($id, $name, $productId, $text, $active)
    {
        $this->findBy(array('id' => $id))->update(array('name' => $name, 'product_id' => $productId, 'text' => $text, 'active' => $active));
    }


    public function setActive($id, $active)
    {
        $this->findBy(array("id" => $id))->update(array("active" => $active));
    }


    public function removeItem($id)
    {
        $this->findBy(array('id' => $id))->delete();
    }
}
