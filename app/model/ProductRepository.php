<?php 
namespace App\Model;

class ProductRepository extends TableManager
{

    private $connection;
    protected $tableName = 'pzshop_products';

    public function getPostsLimited($limit, $offset)
    {
        return $this->findAll()->limit($limit, $offset);
    }

    public function getAllPosts()
    {
        return $this->findAll();
    }

    public function getByName($name)
    {
        return $this->findAll()->fetchPairs($name);
    }

    public function getPostsCount()
    {
        return $this->findAll()->count();
    }

    public function getVisiblePostsCount()
    {
        return $this->findAllVisible()->count();
    }

    public function getById($id)
    {
        return $this->findBy(array('id' => $id));
    }



    public function searchByName($name)
    {
        return $this->findBy(array($name));
    }

    public function getByCategory($id)
    {
        return $this->findBy(array('category' => $id));
    }

    public function addItem($name, $category, $price, $active, $photo, $description, $inStock)
    {
        $query = $this->findAll()->insert(array('name' => $name, 'category' => $category,
         'price' => $price, 'active' => $active, 'photo' => $photo, 'description' => $description, 'in_stock' => $inStock));
    }


    public function editPost($id, $name, $category, $price, $active, $photo, $description, $inStock)
    {
        $this->findBy(array('id' => $id))->update(array('name' => $name, 'category' => $category,
         'price' => $price, 'active' => $active, 'photo' => $photo, 'description' => $description, 'in_stock' => $inStock));
    }

    public function delImg($id, $img)
    {
        $this->findBy(array('id' => $id))->update(array('photo' => null));
    }

    public function updateImg($id, $old, $new)
    {
        $this->findBy(array('id' => $id))->update(array('photo' => $new));
    }

    public function setActive($id, $active)
    {
        $this->findBy(array("id" => $id))->update(array("active" => $active));
    }


    public function removeItem($id)
    {
        $this->findBy(array('id' => $id))->delete();
    }
}
