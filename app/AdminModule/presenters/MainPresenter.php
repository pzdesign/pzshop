<?php

namespace AdminModule;

use Nette;
use Nette\Application\UI;
use Nette\Forms\Controls;
use Nette\Application\UI\Form;
use Nette\Utils\Image;
use Nette\Utils\Strings;
use Nette\Utils\Finder;
use Nette\Utils\Paginator;

class MainPresenter extends BasePresenter
{
    private $paginator;
    private $connection;

    public function startup()
    {
        parent::startup();
        if (!$this->getUser()->isLoggedIn()) {
            $this->redirect('Sign:in');
        }
    }


    public function actionDefault($page = 1)
    {
        $paginator = new Nette\Utils\Paginator;
        $paginator->setItemCount(10);
        $paginator->setItemsPerPage(10);
        $paginator->setPage($page);
        $length = $paginator->getLength();

        if ($page < 1 || $page > $length) {
            //$this->flashMessage("Stránka musí být mezi 1 a $length.");
            $paginator->setPage(1);
        }
        $this->paginator = $paginator;
        $this->template->paginator = $this->paginator;
        $this->template->absImagePath = $this->context->parameters['wwwDir'];

        $allPosts = $this->product->getPostsLimited($this->paginator->getLength(), $this->paginator->getOffset())->order('id DESC');
        $this->template->posts = $allPosts;
    }

    public function renderDefault()
    {
        $this->template->paginator = $this->paginator;
        //$allPosts = $this->product->getPostsLimited($this->paginator->getLength(), $this->paginator->getOffset())->order('id DESC');
        ///$this->template->posts = $allPosts;
    }
}
