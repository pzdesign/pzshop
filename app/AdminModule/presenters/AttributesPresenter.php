<?php

namespace AdminModule;

use Nette;
use Nette\Application\UI;
use Nette\Forms\Controls;
use Nette\Application\UI\Form;
use Nette\Utils\Paginator;

class AttributesPresenter extends BasePresenter
{
    private $paginator;
    private $connection;

    public function startup()
    {
        parent::startup();
        if (!$this->getUser()->isLoggedIn()) {
            $this->redirect('Sign:in');
        }
    }

    public function actionDefault($page = 1)
    {
        $paginator = new Nette\Utils\Paginator;
        $paginator->setItemCount($this->productAttribute->getPostsCount($this->getView()));
        $paginator->setItemsPerPage($this->parameters['pages']);
        $paginator->setPage($page);
        $length = $paginator->getLength();

        if ($page < 1 || $page > $length) {
            $paginator->setPage(1);
        }
        $this->paginator = $paginator;

        $this->template->absImagePath = $this->context->parameters['wwwDir'];
        $this->payload->resetForm = false;

        $products = $this->product->findAll();
        foreach ($products as $key => $product) {
            $productsArray[$product->id] = $product->name;
        }

        $this->template->productName = $productsArray;
    }
    
    public function renderDefault()
    {
        $this->template->posts = $this->productAttribute->getPostsLimited($this->paginator->getLength(), $this->paginator->getOffset())->order('id DESC');
        $this->template->paginator = $this->paginator;
    }


    protected function createComponentAddProductAttributeForm($name)
    {
        $products = $this->product->findBy(array('active' => 1));
        foreach ($products as $key => $product) {
            $productsArraySelect[$product->id] = $product->name;
        }

        $form = new \AddProductAttributeForm($this, $name, $productsArraySelect);
        $form->onSuccess[] = $this->addProductAttributeFormSubmit;
        $form->onValidate[] = $this->addProductAttributeFormValidate;
        return $form;
    }

    //Pridani noveho produktu validace.
    public function addProductAttributeFormValidate(UI\Form $form, $values)
    {
        $this->redrawControl('addProductAttributeFormSnippet');
    }

    //Pridani noveho produktu odeslani.
    public function addProductAttributeFormSubmit($form, $values)
    {
        if ($form->isSuccess()) {
            $values = $form->getValues();

            $this->productAttribute->addItem($values->name, $values->productId, $values->text, $values->active);

            $this->payload->resetForm = true;
            $form->setValues(array(), true);

            $this->paginator->setItemCount($this->productAttribute->getPostsCount());
            $this->redrawControl('postsList');
        }
    }


    protected function createComponentEditProductAttributeForm($name)
    {
        $products = $this->product->findBy(array('active' => 1));
        foreach ($products as $key => $product) {
            $productsArraySelect[$product->id] = $product->name;
        }

        $form = new \EditProductAttributeForm($this, $name, $productsArraySelect);
        $form->addSubmit('close', 'Close')
             ->setValidationScope([])
             ->onClick[] = $this->formCancelled;
        $form->onSuccess[]  = $this->editProductAttributeFormSubmit;
        $form->onValidate[] = $this->editProductAttributeFormValidate;
        return $form;
    }

    //Editace noveho produktu validace.
    public function editProductAttributeFormValidate(UI\Form $form, $values)
    {
        $this->redrawControl('editProductAttributeFormSnippet');
    }

    public function editProductAttributeFormSubmit($form, $values)
    {
        if ($form->isSuccess()) {
            $id = $this->getParameter('id');
            if ($values->active) {
                $values->active = 1;
            } else {
                $values->active = 0;
            }
            $values = $form->getValues();
            $this->productAttribute->editPost($id, $values->name, $values->productId, $values->text, $values->active);
        
            $this->flashMessage("Product $values->name was editted", "success");

            if (!$this->isAjax()) {
                $this->redirect(':Admin:Attributes:');
            }
            $this->redrawControl('flashMessages');
            $this->redirect(':Admin:Attributes:');
        }
    }

    public function formCancelled()
    {
        $this->redirect(':Admin:Attributes:');
    }

    public function actionEdit($id)
    {
        $post = $this->productAttribute->getById($id)->fetch();
        if (!$post) {
            $this->error('Příspěvek nebyl nalezen');
        }
        $this->template->post = $post;

        $this['editProductAttributeForm']->setDefaults($post->toArray());
    }

    public function handleActive($postId)
    {
        if (!is_numeric($postId)) {
            $this->flashMessage("Nehrabej se, prosím, v URL.", "danger");
            return;
        }
        if (!$this->isAjax()) {
            $this->redirect("this");
        }
        $this->productAttribute->setActive($postId, 1);
        $this->flashMessage("Item s ID $postId byl zviditelněn.", "success");
        $this->redrawControl('postsList');
        $this->redrawControl('addProductAttributeFormSnippet');
        $this->redrawControl('flashMessages');
    }

    public function handleInactive($postId)
    {
        if (!is_numeric($postId)) {
            $this->flashMessage("Nehrabej se, prosím, v URL.", "danger");
            return;
        }
        if (!$this->isAjax()) {
            $this->redirect("this");
        }

        $this->productAttribute->setActive($postId, 0);
        $this->flashMessage("Item s ID $postId byl zneviditelněn.", "warning");
        $this->redrawControl("postsList");
        $this->redrawControl('addProductAttributeFormSnippet');
        $this->redrawControl('flashMessages');
    }

    public function handleRemovePost($postId)
    {
        if (!$this->isAjax()) {
            $this->redirect("this");
        }
        $post = $this->productAttribute->getById($postId)->fetch();
        $this->productAttribute->removeItem($postId);
        //$this->payload->deleteItem = true; 
        $this->flashMessage("Success.", "success");
        $this->redrawControl('flashMessages');
        $this->redrawControl('addProductAttributeFormSnippet');
        $this->redrawControl('postsList');
    }

    public function handleChange($page)
    {
        if (!$this->isAjax()) {
            $this->redirect("this");
        }
        $this->paginator->setPage($page);
        $this->redrawControl('postsList');
        return;
    }
}
