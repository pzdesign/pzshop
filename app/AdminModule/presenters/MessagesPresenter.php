<?php

namespace AdminModule;

use Nette;
use Nette\Application\UI;
use Nette\Forms\Controls;
use Nette\Application\UI\Form;
use Nette\Utils\Paginator;
use Nette\Mail\Message;
use Nette\Mail\SendmailMailer;

class MessagesPresenter extends BasePresenter
{
    private $paginator;
    private $connection;

    public function startup()
    {
        parent::startup();
        if (!$this->getUser()->isLoggedIn()) {
            $this->redirect('Sign:in');
        }
    }

    public function actionDefault($page = 1)
    {
        $paginator = new Nette\Utils\Paginator;
        $paginator->setItemCount($this->productMessage->getPostsCount($this->getView()));
        $paginator->setItemsPerPage($this->parameters['pages']);
        $paginator->setPage($page);
        $length = $paginator->getLength();

        if ($page < 1 || $page > $length) {
            $paginator->setPage(1);
        }
        $this->paginator = $paginator;
        $this->template->absImagePath = $this->context->parameters['wwwDir'];

        $products = $this->product->findBy(array('active' => 1));
        foreach ($products as $key => $product) {
            $productsArraySelect[$product->id] = $product->name;
        }
        $this->template->productName = $productsArraySelect;
    }

    public function renderDefault()
    {
        $this->template->posts = $this->productMessage->getPostsLimited($this->paginator->getLength(), $this->paginator->getOffset())->order('id DESC');     
        $this->template->paginator = $this->paginator;
    }

    public function handleActive($postId)
    {
        if (!is_numeric($postId)) {
            $this->flashMessage("Nehrabej se, prosím, v URL.", "danger");
            return;
        }
        if (!$this->isAjax()) {
            $this->redirect("this");
        }

        $this->productMessage->setActive($postId, 1);

        $message = $this->productMessage->getById($postId)->fetch();

        $this->handleSendEmail($postId, $message->email);
        $this->flashMessage("Message was sent to: $message->email.", "success");
        $this->redrawControl('postsList');
        $this->redrawControl('flashMessages');
    }

    public function handleRemovePost($postId)
    {
        if (!$this->isAjax()) {
            $this->redirect("this");
        }
        $post = $this->productMessage->getById($postId)->fetch();
        $this->productMessage->removeItem($postId);
        //$this->payload->deleteItem = true; 
        $this->flashMessage("Success.", "success");
        $this->redrawControl('flashMessages');
        $this->redrawControl('postsList');
    }

    public function handleChange($page)
    {
        if (!$this->isAjax()) {
            $this->redirect("this");
        }
        $this->paginator->setPage($page);
        $this->redrawControl('postsList');
        return;
    }

    public function handleShowDetails($postId)
    {
        if (!is_numeric($postId)) {
            $this->flashMessage("Nehrabej se, prosím, v URL.", "danger");
            return;
        }
        if (!$this->isAjax()) {
            $this->redirect("this");
        }
        $this->flashMessage("Item s ID $postId byl zviditelněn.", "success");
        $this->redrawControl('postsList');
        $this->redrawControl('showDetails');
    }

    public function handleSendEmail($id, $to)
    {
        $mail = new Message;
        $mail->setFrom($this->parameters['emailFrom'])
                 ->addTo($to)
                 ->setSubject('Váš dotaz byl zpracován :');


        $template = $this->createTemplate();
        $template->setFile(__DIR__ . '/../templates/email/emailTemplate.latte');
        $template->title = 'Dotaz o info k produktu';
            
        $mail->setHtmlBody($template);

        $mailer = new SendmailMailer;
        $mailer->send($mail);
    }
}
