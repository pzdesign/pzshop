<?php

namespace AdminModule;

use Nette;
use Nette\Utils\Paginator;
use App\Model\UserManager;
use App\Model\RulesForms;

/**
 * Base presenter for all application presenters.
 */
abstract class BasePresenter extends Nette\Application\UI\Presenter
{

    protected $category;
    protected $product;
    protected $productMessage;
    protected $productAttribute;
    protected $productPhoto;
    protected $parameters;


/**
 * DI table repositories
 * @param  \App\Model\CategoryRepository         $category         pzshop_categories
 * @param  \App\Model\ProductRepository          $product          pzshop_products
 * @param  \App\Model\ProductMessageRepository   $productMessage   pzshop_products_messages
 * @param  \App\Model\ProductAttributeRepository $productAttribute pzshop_products_attributes
 * @param  \App\Model\ProductPhotoRepository     $productPhoto     pzshop_products_photos
 */
    public function inject(
        \App\Model\CategoryRepository $category,
        \App\Model\ProductRepository $product,
        \App\Model\ProductMessageRepository $productMessage,
        \App\Model\ProductAttributeRepository $productAttribute,
        \App\Model\ProductPhotoRepository $productPhoto)
    {
        $this->category = $category;

        $this->product = $product;

        $this->productMessage = $productMessage;
    
        $this->productAttribute = $productAttribute;

        $this->productPhoto = $productPhoto;
    }
    public function startup()
    {
        parent::startup();
        $this->parameters = $this->context->getParameters();

    }
    public function beforeRender()
    {
        $this->setLayout('layout');
        $this->template->parameters = $this->parameters;
    }

    public function handleLogOut()
    {
        $this->user->logout();
        $this->flashMessage('You have been logged out', 'info');
        $this->redirect(':Front:Homepage:default');
    }
}
