<?php

namespace AdminModule;

use Nette;
use Nette\Application\UI;
use Nette\Forms\Controls;
use Nette\Application\UI\Form;
use Nette\Utils\Image;
use Nette\Utils\Strings;
use Nette\Utils\Finder;
use Nette\Utils\Paginator;

class CategoriesPresenter extends BasePresenter
{
    private $paginator;
    private $connection;
    private $categoriesArray = array();


    public function startup()
    {
        parent::startup();
        if (!$this->getUser()->isLoggedIn()) {
            $this->redirect('Sign:in');
        }
    }

    public function actionDefault($page = 1)
    {
        $paginator = new Nette\Utils\Paginator;
        $paginator->setItemCount($this->category->getPostsCount($this->getView()));
        $paginator->setItemsPerPage($this->parameters['pages']);
        $paginator->setPage($page);
        $length = $paginator->getLength();

        if ($page < 1 || $page > $length) {
            //$this->flashMessage("Stránka musí být mezi 1 a $length.");
            $paginator->setPage(1);
        }
        $this->paginator = $paginator;
        $this->template->absImagePath = $this->context->parameters['wwwDir'];
        
        $this->payload->resetForm = false;
        $categories = $this->category->findAll();
        foreach ($categories as $key => $category) {
            $categoriesArray[$category->id] = $category->name;
        }
        $this->template->parentName = $categoriesArray;     
        $this->redrawControl('postsList');

    }
    public function renderDefault()     
    {
        $this->template->posts = $this->category->getPostsLimited($this->paginator->getLength(), $this->paginator->getOffset())->order('id ASC');        
        $this->template->paginator = $this->paginator;    
    }

    public function handleRefreshList()
    {
        $this->paginator->setItemCount($this->category->getPostsCount());
        $this->template->posts = $this->category->getPostsLimited($this->paginator->getLength(), $this->paginator->getOffset())->order('id ASC');        
        $this->redrawControl('postsList');        
    }


    protected function createComponentAddCategoryForm($name)
    {
        $categoriesArray = array();
        $categories = $this->category->findBy(array('active' => 1));
        foreach ($categories as $key => $category) {
            if ($key == 1) {
                $categoriesArray[0] = 'Products';
            }
            $categoriesArray[$category->id] = $category->name;
        }

          //$categories = $this->category->getAllPosts()->fetch();
          //$categoriesArray = $this->categoriesArray;
          //
        $form = new \AddCategoryForm($this, $name, $categoriesArray);
        $form->onSuccess[] = $this->addCategoryFormSubmit;
        $form->onValidate[] =$this->addCategoryFormValidate;
        return $form;
    }

    public function addCategoryFormValidate($form, $values)
    {
        $this->redrawControl('addCategoryFormSnippet');
    }   

    public function addCategoryFormSubmit($form, $values)
    {
        if (!$this->isAjax()) {
            $this->redirect("this");
        }
        if($form->isSuccess()) {
        if ($values->active) {
            $values->active = 1;
        } else {
            $values->active = 0;
        }
        $values = $form->getValues();
        $this->category->addItem($values->name, $values->parent_id, $values->active);

        $this->flashMessage("Success", "success");  
        $this->redrawControl('flashMessages');
        $this->payload->resetForm = true;
        $form->setValues(array(), true);
        $this->paginator->setItemCount($this->category->getPostsCount());
        } 
    }

 

    protected function createComponentEditCategoryForm($name)
    {
        $id = $this->getParameter('id');
        $categoriesArray = array();
        $categories = $this->category->findBy(array('active' => 1));
        foreach ($categories as $key => $category) {
            if ($key == 1) {
                $categoriesArray[0] = 'Products';
            }
            $categoriesArray[$category->id] = $category->name;
        }
        // zjisti potomky
        $mamPotomky = $this->maPotomky($id);
        $tyhleNe = array();
        
        foreach ($mamPotomky as $potomek) {
            $tyhleNe[$potomek->id] = $potomek->parent_id;
        }
           //print_r($tyhleNe); 
        $form = new \EditCategoryForm($this, $name, $categoriesArray);
        $form->addSubmit('close', 'Close')
             ->setValidationScope([])
             ->onClick[] = [$this, 'formCancelled'];
        $form->onSuccess[] = $this->editCategoryFormSubmit;
        $form->onValidate[] = $this->editCategoryFormValidate;
        return $form;
    }
    public function editCategoryFormValidate($form, $values)
    {
        $this->redrawControl('editCategoryFormSnippet');
    }

    public function editCategoryFormSubmit($form, $values)
    {
        $id = $this->getParameter('id');
        if ($values->active) {
            $values->active = 1;
        } else {
            $values->active = 0;
        }
        $values = $form->getValues();

        $this->flashMessage("", "danger");
        $this->redrawControl('flashMessages');

        // zjisti potomky
        $mamPotomky = $this->maPotomky($id);
        $tyhleNe = array();
        foreach ($mamPotomky as $potomek) {
            $tyhleNe[$potomek->id] = $potomek->id;
        }
        $jsouVpoli = in_array($values->parent_id, $tyhleNe);

        //$e = json_encode($tyhleNe);
        if ($values->parent_id == $id || $jsouVpoli) {
            $form['parent_id']->addError('cant do that!');
            $this->flashMessage("Cant be parent of myself!.", "danger");
            $this->redrawControl('flashMessages');
            $this->redrawControl('editCategorySnippet');
        } else {
            if ($values->parent_id == 0) {
                $values->parent_id = null;
            }
            $this->category->editPost($id, $values->name, $values->parent_id, $values->active);
            $this->flashMessage("SUCCESS.", "success");
            $this->redrawControl('flashMessages');
        }
    
        $this->redrawControl('postsList');
    }

    public function formCancelled()
    {
        $this->redirect(':Admin:Categories:');
    }

    public function actionEdit($id)
    {
        $this->template->absImagePath = $this->context->parameters['wwwDir'];
        $allActiveCategories = $this->category->findBy(array('active' => 1));

        $tytoMam = array();
        foreach ($allActiveCategories as $category) {
            $tytoMam[$category->id] = $category->id;
        }

        $post = $this->category->getById($id)->fetch();
        if (!$post) {
            $this->error('Příspěvek nebyl nalezen');
        } else {
            $this->template->post = $post;
            if (in_array($post->parent_id, $tytoMam)) {
                $this['editCategoryForm']->setDefaults($post->toArray());
            } else {
                $this['editCategoryForm']['parent_id']->setDefaultValue(null);
                $this['editCategoryForm']['name']->setDefaultValue($post->name);
                $this['editCategoryForm']['active']->setDefaultValue($post->active);
            }
        }
    }
    public function handleActive($postId)
    {
        if (!is_numeric($postId)) {
            $this->flashMessage("Nehrabej se, prosím, v URL.", "danger");
            return;
        }
        if (!$this->isAjax()) {
            $this->redirect("this");
        }

        $this->category->setActive($postId, 1);
        $this->flashMessage("Item s ID $postId byl zviditelněn.", "success");
        $this->redrawControl('postsList');
        $this->redrawControl('addCategorySnippet');
        $this->redrawControl('flashMessages');
    }

    public function handleInactive($postId)
    {
        if (!is_numeric($postId)) {
            $this->flashMessage("Nehrabej se, prosím, v URL.", "danger");
            return;
        }
        if (!$this->isAjax()) {
            $this->redirect("this");
        }

        $this->category->setActive($postId, 0);
        $this->flashMessage("Item s ID $postId byl zneviditelněn.", "warning");
        $this->redrawControl("postsList");
        $this->redrawControl('addCategorySnippet');
        $this->redrawControl('flashMessages');
    }

    public function handleRemovePost($postId)
    {
        if (!$this->isAjax()) {
            $this->redirect("this");
        }
        $post = $this->category->getById($postId)->fetch();
        $this->category->removeItem($postId);
        //$this->payload->deleteItem = true; 
        $this->flashMessage("Success.", "success");
        $this->redrawControl('flashMessages');
        $this->redrawControl('addCategorySnippet');
        $this->redrawControl('postsList');
    }

    public function handleChange($page)
    {
        if (!$this->isAjax()) {
            $this->redirect("this");
        }
        $this->paginator->setPage($page);
        $this->redrawControl('postsList');
        return;
    }

    public function maPotomky($id)
    {
        $deti = array();
        $deti = $this->category->findBy(array('parent_id' => $id));
        $this->template->maPotomky = true;

        return $deti;
    }
}
