<?php

namespace AdminModule;

use Nette;
use Nette\Application\UI;
use Nette\Forms\Controls;
use Nette\Application\UI\Form;
use Nette\Utils\Image;
use Nette\Utils\Strings;
use Nette\Utils\Finder;
use Nette\Utils\Paginator;

class ProductsPresenter extends BasePresenter
{
    private $paginator;
    private $connection;
    private $categoriesArray = array();
    private $potomciKategorii = array();
    private $asc = false;
    public function startup()
    {
        parent::startup();
        if (!$this->getUser()->isLoggedIn()) {
            $this->redirect('Sign:in');
        }
    }

    public function actionDefault($page = 1)
    {
        $paginator = new Nette\Utils\Paginator;
        $paginator->setItemCount($this->product->getPostsCount($this->getView()));
        $paginator->setItemsPerPage($this->parameters['pages']);
        $paginator->setPage($page);
        $length = $paginator->getLength();

        if ($page < 1 || $page > $length) {
            $paginator->setPage(1);
        }
        $this->paginator = $paginator;
        $this->template->absImagePath = $this->context->parameters['wwwDir'];

        $categoriesArray = array();

        $categories = $this->category->findAll();
        $this->template->categories = $categories;
        foreach ($categories as $key => $category) {
            $categoriesArray[$category->id] = $category->name;
        }
        $this->template->parentName = $categoriesArray;
        //pomocne promenne pro nahrani fota
        $this->template->posts = $this->product->getPostsLimited($this->paginator->getLength(), $this->paginator->getOffset())->order("id DESC"); 

        $this->asc = false;
        $this->payload->resetForm = false;
        $this->template->productPhotoName = 'empty';
        $this->template->productPhotoFormUploadClicked = false;
        $this->template->productPhotoFormUploadDone = false;
        $this->template->productPhotoFormUploadFail = false;
        $this->template->productPhotoRemoved = false;
        $this->template->productPhotoRemovedInfo = false;
    }
    public function renderDefault()
    {
        $this->template->paginator = $this->paginator;
    }

    protected function createComponentAddProductForm($name)
    {
        $categoriesArray = array();
        $categories = $this->category->findBy(array('active' => 1));
        foreach ($categories as $key => $category) {
            $categoriesArray[$category->id] = $category->name;
        }

        $form = new \AddProductForm($this, $name, $categoriesArray);
        $form->onSuccess[] = $this->addProductFormSubmit;
        $form->onValidate[] = $this->addProductFormValidate;
        return $form;
    }

    //Pridani noveho produktu validace.
    public function addProductFormValidate(UI\Form $form, $values)
    {
        $this->redrawControl('addProductFormSnippet');
        if (!$values->productPhotoInForm) {
            $this['productPhotoFormUpload']['productPhoto']->addError('Must be filled');
            $this->redrawControl('productPhotoFormUploadSnippet');
        }
    }

    //Pridani noveho produktu odeslani.
    public function addProductFormSubmit($form, $values)
    {
        if ($form->isSuccess()) {
            $values = $form->getValues();

            $values->productPhotoInForm = str_replace('storage/', '', $values->productPhotoInForm);

            $this->product->addItem($values->name, $values->category, $values->price, $values->active,
            $values->productPhotoInForm, $values->description, $values->inStock);

            $this->paginator->setItemCount($this->product->getPostsCount());

            $this->payload->resetForm = true;
            $form->setValues(array(), true);

            $this->handleProductPhotoSave();
            $this->paginator->setItemCount($this->product->getPostsCount());

            $this->redrawControl('postsList');
            $this->payload->resetForm = true;
            $this['productPhotoFormUpload']->setValues(array(), true);
            $this->redrawControl('productPhotoFormUploadSnippet');
        }
    }

    protected function createComponentProductPhotoFormUpload()
    {
        $form = new Form;
        $form->addUpload("productPhoto", "Product photo:")
                ->addRule(Form::IMAGE, "Only img with .jpg and .png can be selected")
                ->setAttribute('placeholder', 'napište email');

        $form->addSubmit("productPhotoUploadSubmit", "Upload");

        $form->onValidate[] = $this->productPhotoFormUploadCheck;
        $form->onSuccess[] = $this->productPhotoFormUploadSubmit;
        return $form;
    }

    public function productPhotoFormUploadCheck(UI\Form $form, $values)
    {
        $this->redrawControl('productPhotoFormUploadSnippet');
    }

    public function productPhotoFormUploadSubmit(UI\Form $form, $values)
    {
        if (!$this->isAjax()) {
            $this->redirect('this');
        }
            
        $values = $form->getValues();

        $dir = 'upload/products/storage/';
        $uploadDir = 'upload/products/';

        $name = $values->productPhoto->getName();
        $path = $dir.$name;
        $ext = '.jpg';

        $explode = explode(".", $name);
        $filetype = $explode[1];

        if ($filetype == 'jpg') {
            $ext = '.jpg';
        } elseif ($filetype == 'jpeg') {
            $ext = '.jpeg';
        } elseif ($filetype == 'png') {
            $ext = '.png';
        } elseif ($filetype == 'gif') {
            $ext = '.gif';
        }

        $name = str_replace($ext, '', $name);
        $name = Strings::webalize($name);
        $suffix = Strings::random(9);
        $name = $suffix;
        $path = $dir.$name.$ext;
        $uploadDir = $uploadDir.$name.$ext;

        if (file_exists($path) || file_exists($uploadDir)) {
            $form['productPhoto']->addError("Photo already exists!");
            $this->template->productPhotoFormUploadFail = true;
            $this->redrawControl('flashMessages');
            $this->redrawControl('productPhotoFormUploadSnippet');
        } else {
            $values->productPhoto->move($path);
            $image = Image::fromFile($path);
            $image->resize(320, 180);
            $image->sharpen();
            $image->save($path, 80, Image::JPEG);
            //$this->redrawControl('flashMessages');
            $this->flashMessage("Foto bylo nahráno", "success");
            $this->template->productPhotoFormUploadClicked = true;
            $this->template->productPhotoFormUploadDone = true;
            $this->template->productPhotoName = $path;
            $this->redrawControl('productPhotoFormUploadSnippet');
        }
    }



    protected function createComponentEditProductForm($name)
    {
        $categoriesArray = array();
        $categories = $this->category->findAll();
        foreach ($categories as $key => $category) {
            $categoriesArray[$category->id] = $category->name;
        }

        $form = new \EditProductForm($this, $name, $categoriesArray);
        $form->addSubmit('close', 'Close')
             ->setValidationScope([])
             ->onClick[] = $this->formCancelled;
        $form->onSuccess[]  = $this->editProductFormSubmit;
        $form->onValidate[] = $this->editProductFormValidate;
        return $form;
    }

    //Editace noveho produktu validace.
    public function editProductFormValidate(UI\Form $form, $values)
    {
        $this->redrawControl('editProductFormSnippet');
        $this->redrawControl('productPhotoFormUploadSnippet');
        if (!$values->productPhotoInForm) {
            $this['productPhotoFormUpload']['productPhoto']->addError('Must be filled');
            $this->redrawControl('productPhotoFormUploadSnippet');
        }
    }

    public function editProductFormSubmit($form, $values)
    {
        if ($form->isSuccess()) {
            $id = $this->getParameter('id');
            if ($values->active) {
                $values->active = 1;
            } else {
                $values->active = 0;
            }
            $values = $form->getValues();
            $values->productPhotoInForm = str_replace('storage/', '', $values->productPhotoInForm);
            $this->product->editPost($id, $values->name, $values->category, $values->price, $values->active, $values->productPhotoInForm,
         $values->description, $values->inStock);
        
            $this->flashMessage("Product $values->name was editted", "success");

            if (!$this->isAjax()) {
                $this->redirect(':Admin:Players:');
            }
            $this->handleSaveProductPhotoInEditMode();
            $this->redrawControl('productPhotoFormUploadSnippet');
            $this->redrawControl('flashMessages');
            $this->redirect(':Admin:Products:');
        }
    }

    public function formCancelled()
    {
        $this->redirect(':Admin:Products:');
    }

    public function actionEdit($id)
    {
        $this->template->absImagePath = $this->context->parameters['wwwDir'];
        //pomocne promenne pro upload 
        $this->template->absImagePath = $this->context->parameters['wwwDir'];
        $this->template->productPhotoFormUploadClicked = false;
        $this->template->productPhotoFormUploadDone = false;
        $this->template->productPhotoFormUploadFail = false;
        $this->template->productPhotoFormEdited = false;
        $this->template->productPhotoRemoved = false;
        $this->template->productPhotoRemovedInfo = false;
        $this->payload->closeForm = false;

        $productAttributes = $this->productAttribute->findBy(array('product_id' => $id, 'active' => 1));
        $this->template->attributes = $productAttributes;

        $post = $this->product->getById($id)->fetch();
        if (!$post) {
            $this->error('Příspěvek nebyl nalezen');
        }
        $this->template->post = $post;

        $this['editProductForm']->setDefaults($post->toArray());
    }

    public function handleActive($postId)
    {
        if (!is_numeric($postId)) {
            $this->flashMessage("Nehrabej se, prosím, v URL.", "danger");
            return;
        }
        if (!$this->isAjax()) {
            $this->redirect("this");
        }
        $this->product->setActive($postId, 1);
        $this->flashMessage("Item s ID $postId byl zviditelněn.", "success");
        $this->redrawControl('postsList');
        $this->redrawControl('addProductFormSnippet');
        $this->redrawControl('flashMessages');
    }

    public function handleInactive($postId)
    {
        if (!is_numeric($postId)) {
            $this->flashMessage("Nehrabej se, prosím, v URL.", "danger");
            return;
        }
        if (!$this->isAjax()) {
            $this->redirect("this");
        }

        $this->product->setActive($postId, 0);
        $this->flashMessage("Item s ID $postId byl zneviditelněn.", "warning");
        $this->redrawControl("postsList");
        $this->redrawControl('addProductFormSnippet');
        $this->redrawControl('flashMessages');
    }

    public function handleRemovePost($postId)
    {
        if (!$this->isAjax()) {
            $this->redirect("this");
        }
        $post = $this->product->getById($postId)->fetch();
        $this->product->removeItem($postId);
        //$this->payload->deleteItem = true; 
        $this->flashMessage("Success.", "success");
        $this->redrawControl('flashMessages');
        $this->redrawControl('addProductFormSnippet');
        $this->redrawControl('postsList');
    }

    public function handleChange($page)
    {
        if (!$this->isAjax()) {
            $this->redirect("this");
        }
        $this->paginator->setPage($page);
        $this->redrawControl('postsList');
        return;
    }

    public function renderSearch($name)
    {
        $products = $this->product->findBy(array('name LIKE ?' => "%$name%"));
        $this->template->products = $products;

        $categories = $this->category->findBy(array('active' => 1));
        $this->template->categories = $categories;
    }




    protected function createComponentSearchForm()
    {
        $categories = $this->category->findBy(array('active' => 1));
        $categoriesSelect = array('All Categories');
        foreach ($categories as $key => $category) {
            $categoriesSelect[$category->id] = $category->name;
        }
        //print_r($categoriesSelect);
        $form = new UI\Form;
        $form->addText('search', 'Product name:')->setDefaultValue(null);
        $form->addSelect('categories', 'Category: ', $categoriesSelect);
        $form->addSubmit('find', 'Show');
        $form->onSuccess[] = $this->searchFormSubmit;
        return $form;
    }

    // volá se po úspěšném odeslání formuláře
    public function searchFormSubmit(UI\Form $form, $values)
    {
        $values = $form->getValues();
        $this->handleSearch($values->search, $values->categories);

        $this->redrawControl('postsList');
    }

    public function handleSearch($name, $category = 0)
    {
        if (!$this->isAjax()) {
            $this->redirect("this");
        }

        if ($category !== 0) {
            $childs = $this->maPotomkyPotomku($category);
            $childs2 = array_unshift($this->potomciKategorii, $category);
            $test = $this->potomciKategorii;
            if (!$name) {
                $products = $this->product->findBy(array('category' => $test));
            } elseif ($name) {
                $products = $this->product->findBy(array('category' => $test, 'name LIKE ?' => "%$name%"));
            }
        } elseif ($category == 0 && $name == null) {
            $products = $this->product->findAll();
        } else {
            $products = $this->product->findBy(array('name LIKE ?' => "%$name%"));
        }

        $this->template->posts = $products;
        $this->redrawControl('postsList');

        $this->redrawControl('flashMessages');
    }

    public function maPotomkyPotomku($id, $parent_id = 0, $level =0)
    {
        $childs = array($id);
        $childs = $this->category->findBy(array('parent_id' => $id));
        foreach ($childs as $key => $child) {
            if ($child->parent_id == $id) {
                $this->potomciKategorii[] = $child->id;
                $this->maPotomkyPotomku($child->id);
            }
        }
        //print_r($potomci); 
        return $this->potomciKategorii;
    }

    public function handleProductPhotoSave()
    {
        $dir = 'upload/products/storage/';
        $dirNew = 'upload/products/';

        if (!$this->isAjax()) {
            $this->redirect("this");
        }
     
        foreach (Finder::findFiles('*.jpg', '*.png', '*.JPG', '*.PNG')->in($dir) as $key => $file) {
            $nfile = str_replace('/storage', '', $file);
            if (!file_exists($nfile)) {
                copy($file, $nfile);
                unlink($file);
                $this->flashMessage("SUCCESS", "success");
                $this->redrawControl('flashMessages');
                $this->redrawControl('postsList');
            } else {
                $this->flashMessage("Naše databáze již obsahuje tento soubor", "danger");
                $this->redrawControl('flashMessages');
            }
        }
    }

    public function handleRemoveCacheImg($imgRealName)
    {
        if (!$this->isAjax()) {
            $this->redirect("this");
        }
        if (file_exists($imgRealName)) {
            unlink($imgRealName);
            $this->template->productPhotoFormUploadClicked = false;
            $this->template->productPhotoFormUploadDone = false;
            $this->template->productPhotoRemoved = true;
            $this->template->productPhotoRemovedInfo = true;
            $this->redrawControl('flashMessages');
            $this->redrawControl('productPhotoFormUploadSnippet');
        } else {
            $this->flashMessage("CHYBA.", "danger");
        }
        return;
    }

    public function handleRemoveImg($postId)
    {
        if (!$this->isAjax()) {
            $this->redirect("this");
        }
        $post = $this->product->getById($postId)->fetch();

        if (file_exists($post->photo)) {
            $this->flashMessage("Photo was deleted.", "success");
            $this->product->delImg($postId, $post->photo);

            unlink($post->photo);
        } else {
            $this->flashMessage("ERROR.", "danger");
        }
        //unlink($post->img);        
        $this->template->removeDone = true;
        $this->template->productPhotoRemoved = true;
        $this['editProductForm']['productPhotoInForm']->setDefaultValue('toto');
        $this->redrawControl("productPhotoFormUploadSnippet");
        $this->redrawControl("editProductFormSnippet");
        $this->redrawControl("flashMessages");
        return;
    }


    public function handleSaveProductPhotoInEditMode()
    {
        $postId = $this->getParameter("id");
        $dir = 'upload/products/storage';
        $dirNew = 'upload/products/';
        if ($postId) {
            $post = $this->product->getById($postId)->fetch();
        }

        if (!$this->isAjax()) {
            $this->redirect("this");
        }

        foreach (Finder::findFiles('*')->in($dir) as $key => $file) {
            $nfile = str_replace('/storage', '', $file);
            if (!file_exists($nfile)) {
                if ($postId) {
                    $this->product->updateImg($postId, $file, $nfile);
                    copy($file, $nfile);
                    unlink($file);
                    $this->template->productPhotoFormEdited = true;
                    //$this->template->postAdded = true;
                    $this->flashMessage("SUCCESS", "success");
                    $this->redirect(':Admin:Products:');
                    $this->redrawControl('productPhotoFormUploadSnippet');
                    $this->redrawControl('flashMessages');
                }
            } else {
                $this->flashMessage("Naše databáze již obsahuje tento soubor", "danger");
            }
        }
    }

    protected function createComponentAddNewProductAttributeForm()
    {
        $id = $this->getParameter('id');

        $form = new Form;
        $form->addText('name', 'Name: ')->addRule($form::MIN_LENGTH, 'Name must contain atleast %d words', 3)
        ->addRule($form::MAX_LENGTH, 'Name is too long', 50)->setRequired('Please enter name.');
        $form->addHidden('productId', 'Product: ')->setDefaultValue($id);
        $form->addText('text', 'Text: ')->addRule($form::MIN_LENGTH, 'Text must contain atleast %d words', 3);
        $form->addCheckbox('active', 'Active: ');
        $form->addSubmit('addAttribute', 'Add');
        $form->onSuccess[] = $this->addNewProductAttributeFormSubmit;
        $form->onValidate[] = $this->addNewProductAttributeFormValidate;
        return $form;
    }

    //Pridani noveho produktu validace.
    public function addNewProductAttributeFormValidate(UI\Form $form, $values)
    {
        $this->redrawControl('addNewProductAttributeFormSnippet');
    }

    //Pridani noveho produktu odeslani.
    public function addNewProductAttributeFormSubmit($form, $values)
    {
        if ($form->isSuccess()) {
            $values = $form->getValues();
            $this->productAttribute->addItem($values->name, $values->productId, $values->text, $values->active);

            $this->payload->resetForm = true;

            $form->setValues(array(), true);

            $this->flashMessage("Success", "success");
            $this->redrawControl('flashMessages');

            $this->redrawControl('addProductFormSnippet');
            $this->redrawControl('editProductFormSnippet');
        }
    }



    public function handleRemoveAttributePost($postId)
    {
        if (!$this->isAjax()) {
            $this->redirect("this");
        }
        $post = $this->productAttribute->getById($postId)->fetch();
        $this->productAttribute->removeItem($postId);
        //$this->payload->deleteItem = true; 
        $this->flashMessage("Success.", "success");
        $this->redrawControl('flashMessages');
        $this->redrawControl('editProductFormSnippet');
        $this->redrawControl('postsList');
    }
}
