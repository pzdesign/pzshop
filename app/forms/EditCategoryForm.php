<?php

use Nette\Application\UI;
use Nette\ComponentModel\IContainer;
use Nette\Application\UI\Form;

class EditCategoryForm extends UI\Form
{

    public function __construct(IContainer $parent = null, $name = null, $categories)
    {
        parent::__construct($parent, $name, $categories);	

        $this->addText('name', 'Name: ')->addRule($this::MIN_LENGTH, 'Name must contain atleast %d words', 2)
        ->addRule($this::MAX_LENGTH, 'Name is too long', 50)->setRequired('Please enter name.');
        
        $this->addSelect('parent_id', 'Parent: ',$categories)->setAttribute('id', 'parent');        
        $this->addCheckbox('active', 'Active: ');
        $this->addSubmit('submit', 'Save');

    }
}
