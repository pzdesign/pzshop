<?php

use Nette\Application\UI;
use Nette\ComponentModel\IContainer;
use Nette\Application\UI\Form;

class EditProductAttributeForm extends UI\Form
{

    public function __construct(IContainer $parent = null, $name = null, $products)
    {
        parent::__construct($parent, $name, $products); 

        $this->addText('name', 'Name: ')->addRule($this::MIN_LENGTH, 'Name must contain atleast %d words', 1)
        ->addRule($this::MAX_LENGTH, 'Name is too long', 50)->setRequired('Please enter name.');
        $this->addSelect('productId', 'Product: ',$products)->setAttribute('id', 'productId');        
        $this->addText('text', 'Text: ')->addRule($this::MIN_LENGTH, 'Text must contain atleast %d words', 1);
        $this->addCheckbox('active', 'Active: ');      
        $this->addSubmit('submit', 'Save');
    }
}
