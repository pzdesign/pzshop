<?php

use Nette\Application\UI;
use Nette\ComponentModel\IContainer;
use Nette\Application\UI\Form;

class AddProductForm extends UI\Form
{

    public function __construct(IContainer $parent = null, $name = null, $categories)
    {
        parent::__construct($parent, $name, $categories);	

        $this->addText('name', 'Name: ')->addRule($this::MIN_LENGTH, 'Name must contain atleast %d words', 3)
        ->addRule($this::MAX_LENGTH, 'Name is too long', 30)->setRequired('Please enter name.');
        $this->addSelect('category', 'Category: ',$categories)->setAttribute('id', 'category');        
        $this->addText('price', 'Price: ')->addRule($this::FLOAT, 'Only numbers', 30)->setRequired('Please enter price.')->setDefaultValue(0);
        $this->addText('inStock', 'In stock: ')->addRule($this::FLOAT, 'Only numbers', 5)->setDefaultValue(0);        
        $this->addTextArea('description', 'Description: ');
        $this->addCheckbox('active', 'Active: ');
        $this->addText("productPhotoInForm")->setAttribute('id', 'productPhoto')->setAttribute('readonly')
             ->setDefaultValue(null)->addRule(Form::FILLED, 'Must be filled');        
        $this->addSubmit('submit', 'Add');
    }
}
