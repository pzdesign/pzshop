<?php

use Nette\Application\UI;
use Nette\ComponentModel\IContainer;
use Nette\Application\UI\Form;

class AddCategoryForm extends UI\Form
{

    public function __construct(IContainer $parent = null, $name = null, $categories)
    {
        parent::__construct($parent, $name, $categories);	

        $this->addText('name', 'Name: ')->addRule($this::MAX_LENGTH, 'Name is too long', 30)->setRequired('Please enter name.');
        $this->addSelect('parent_id', 'Parent: ',$categories)->setAttribute('id', 'parent');        
        $this->addCheckbox('active', 'Active: ');
        $this->addSubmit('submit', 'Add');
    }
}
