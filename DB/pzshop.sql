-- phpMyAdmin SQL Dump
-- version 4.4.14
-- http://www.phpmyadmin.net
--
-- Počítač: 127.0.0.1
-- Vytvořeno: Čtv 14. dub 2016, 01:27
-- Verze serveru: 5.6.26
-- Verze PHP: 5.6.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Databáze: `pzshop`
--

-- --------------------------------------------------------

--
-- Struktura tabulky `pzshop_categories`
--

CREATE TABLE IF NOT EXISTS `pzshop_categories` (
  `id` int(11) NOT NULL,
  `name` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `slug` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `active` int(11) NOT NULL,
  `reserve` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=49 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Vypisuji data pro tabulku `pzshop_categories`
--

INSERT INTO `pzshop_categories` (`id`, `name`, `parent_id`, `slug`, `active`, `reserve`) VALUES
(1, 'Keyboards', NULL, 'keyboard', 1, 1),
(2, 'Mice', 1, 'mice', 1, 1),
(3, 'Headphones', NULL, 'headphones', 1, 1),
(4, 'Other', NULL, 'other', 1, 1),
(5, 'Wireless-keyboards', 1, 'wireless-keyboards', 1, 1),
(7, 'Earphones', 3, 'earphones', 1, 1),
(8, 'Headset', 3, '', 1, 0),
(9, 'Mechanical', 1, '', 1, 0),
(19, 'Optical', 2, '', 1, 0),
(23, 'Laser mice', 2, '', 1, 0),
(26, 'newProducts', NULL, '', 1, 1),
(28, 'TOP', NULL, '', 1, 0),
(29, 'CZ', 9, '', 1, 0),
(30, 'US', 9, '', 1, 0);

-- --------------------------------------------------------

--
-- Struktura tabulky `pzshop_products`
--

CREATE TABLE IF NOT EXISTS `pzshop_products` (
  `id` int(11) NOT NULL,
  `name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `description` longtext COLLATE utf8_unicode_ci,
  `category` int(11) DEFAULT NULL,
  `price` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `in_stock` int(11) DEFAULT NULL,
  `photo` text COLLATE utf8_unicode_ci,
  `active` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=51 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Vypisuji data pro tabulku `pzshop_products`
--

INSERT INTO `pzshop_products` (`id`, `name`, `description`, `category`, `price`, `in_stock`, `photo`, `active`) VALUES
(2, 'Optical mouse', 'Mouse 2 descMouse 2 descMouse 2 descMouse 2 descMouse 2 descMouse 2 descMouse 2 descMouse 2 descMouse 2 descMouse 2 descMouse 2 descMouse 2 descMouse 2 descMouse 2 descMouse 2 descMouse 2 descMouse 2 descMouse 2 descMouse 2 descMouse 2 descMouse 2 desc', 19, '60', 0, 'upload/products\\c5v572pux.jpg', 1),
(3, 'Mechanical Keyboard', 'Mouse 3 desc', 9, '40', 20, 'upload/products\\fsvp63udr.jpg', 1),
(4, 'Laser mouse', 'Mouse 4 desc', 23, '20', 20, 'upload/products\\j9x6i4osj.jpg', 1),
(7, 'SteelSeries', '', 8, '50', 0, NULL, 1),
(8, 'inEarSound', '', 7, '150', 0, 'upload/products\\szemq7vbh.jpg', 1),
(12, 'zuzuzu', '', 2, '40', 0, NULL, 1),
(16, 'Kraken', '', 3, '20', 0, NULL, 1),
(17, 'Wireless mouse', '', 2, '60', 0, NULL, 1),
(18, 'LeftHandedMice', '', 2, '60', 0, NULL, 1),
(19, 'newProductInNewProducts', '', 26, '60', 0, NULL, 1),
(21, 'Wireless Keyboard', 'asdasd', 5, '20', 0, 'upload/products\\rdv87oqe8.jpg', 1),
(22, 'WirelessMouse 2.0', '', 2, '90', 0, NULL, 1),
(24, 'LaserMouse 2.0', '', 2, '60', 0, NULL, 1),
(25, 'WirelessMouse 3.0', '', 2, '99', 0, NULL, 1),
(28, 'MousePad 1.0', '', 4, '10', 0, NULL, 1),
(29, 'MousePad 2.0', '', 4, '20', 0, NULL, 1),
(30, 'MousePad 3.0', '', 4, '30', 0, NULL, 1),
(31, 'MousePad 4.0', '', 4, '60', 0, NULL, 1),
(32, 'yoga', '', 1, '20', 0, NULL, 1),
(34, 'tulipany', '', 1, '60', 0, 'upload/products/tulips.jpg', 1),
(35, 'tucnaci', '', 2, '100', 0, 'upload/products/penguins.jpg', 1),
(36, 'koala', '', 1, '100', 0, 'upload/products/koala.jpg', 1),
(38, 'Headsets', '', 1, '30', 0, 'upload/products/hydrangeas.jpg', 1),
(39, 'tucnaci', '', 2, '10', 0, 'upload/products\\sv66tgfgi.jpg', 1),
(40, 'Mechanical Keyboard 1.0', '', 29, '1000', 0, 'upload/products/6si9naz9m.jpg', 1),
(41, 'ASas', 'asdasd asd sadsaasd dd dasd .asdasd asd sadsaasd dd dasd .asdasd asd sadsaasd dd dasd .asdasd asd sadsaasd dd dasd .asdasd asd sadsaasd dd dasd .asdasd asd sadsaasd dd dasd .', 28, '33', 3, 'upload/products/6q8u76qj4.jpg', 1),
(42, 'asdasd', '', 28, '20.99', 0, 'upload/products/71pxkgh8j.jpg', 1),
(48, 'asdfasfdgdsgf', '', 1, '100', 0, 'upload/products/75pae2hq7.jpg', 1),
(50, 'asdasd', '', 1, '50', 0, 'upload/products/ntifovrvl.jpg', 1);

-- --------------------------------------------------------

--
-- Struktura tabulky `pzshop_product_attributes`
--

CREATE TABLE IF NOT EXISTS `pzshop_product_attributes` (
  `id` int(11) NOT NULL,
  `name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `product_id` int(11) DEFAULT NULL,
  `text` text COLLATE utf8_unicode_ci NOT NULL,
  `active` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Vypisuji data pro tabulku `pzshop_product_attributes`
--

INSERT INTO `pzshop_product_attributes` (`id`, `name`, `product_id`, `text`, `active`) VALUES
(2, 'Buttons', 2, '5', 1),
(3, 'DPI', 4, '1000', 1),
(13, 'asdasd', 2, 'asdsad', 1),
(20, 'Weight', 4, '100g', 1),
(21, 'DolbyDigital', 8, 'yes', 1),
(22, 'DPI', 2, '1800', 1),
(23, 'Switches', 21, 'Cherry MX Red', 1),
(26, 'DPI', 41, '400', 1),
(27, 'Stereo', 50, 'Yes', 1),
(28, 'CPI', 41, '200', 1);

-- --------------------------------------------------------

--
-- Struktura tabulky `pzshop_product_messages`
--

CREATE TABLE IF NOT EXISTS `pzshop_product_messages` (
  `id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `email` text COLLATE utf8_unicode_ci,
  `name` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `phone` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `body` text COLLATE utf8_unicode_ci NOT NULL,
  `product_id` int(11) DEFAULT NULL,
  `confirmed` int(11) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Vypisuji data pro tabulku `pzshop_product_messages`
--

INSERT INTO `pzshop_product_messages` (`id`, `created_at`, `email`, `name`, `phone`, `body`, `product_id`, `confirmed`) VALUES
(1, '2016-04-13 13:56:25', 'spell.lotr2@gmail.com', 'Patrik', 'spell.lotr2@gmail.co', 'Text', 40, 1),
(2, '2016-04-13 18:14:36', 'spell.lotr2@gmail.com', 'Patrik Žižka', 'spell.lotr2@gmail.co', 'asdasdasd', 41, 1),
(4, '2016-04-13 19:03:06', 'patrik.zizka.s@gmail.com', 'cesko', 'patrik.zizka.s@gmail', '12242', 41, 0),
(5, '2016-04-13 19:05:00', 'spell.lotr2@gmail.com', 'Patrik Žižka', 'spell.lotr2@gmail.com', '6464465', 8, 0);

-- --------------------------------------------------------

--
-- Struktura tabulky `pzshop_product_photos`
--

CREATE TABLE IF NOT EXISTS `pzshop_product_photos` (
  `id` int(11) NOT NULL,
  `product_id` int(11) DEFAULT NULL,
  `src` text COLLATE utf8_unicode_ci NOT NULL,
  `active` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Struktura tabulky `pzshop_users`
--

CREATE TABLE IF NOT EXISTS `pzshop_users` (
  `id` int(11) NOT NULL,
  `username` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `ip` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `role` int(11) NOT NULL,
  `active` int(11) NOT NULL,
  `reserve` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Vypisuji data pro tabulku `pzshop_users`
--

INSERT INTO `pzshop_users` (`id`, `username`, `password`, `email`, `ip`, `role`, `active`, `reserve`) VALUES
(1, 'guest', '$2y$10$Fp6p.o6gptXQkLDPIBnuu.dMJxkZEh8ZJdiUTZIxQ3nQ2B/Ejb9ca', '', '', 0, 1, 0),
(2, 'user', '$2y$10$0qEXKC7yBzjoB1I5WIzIHO.M5xD7x/AvtMbbJE2.taybwriU5DXs.', '', '', 1, 1, 0),
(3, 'admin', '$2y$10$YlL1sbGkfGi1ie6lPjOb9eO9i7rwKEI.Tczqip0IPEaQJe0vBnTzS', '', '', 2, 1, 0);

--
-- Klíče pro exportované tabulky
--

--
-- Klíče pro tabulku `pzshop_categories`
--
ALTER TABLE `pzshop_categories`
  ADD PRIMARY KEY (`id`);

--
-- Klíče pro tabulku `pzshop_products`
--
ALTER TABLE `pzshop_products`
  ADD PRIMARY KEY (`id`),
  ADD KEY `category` (`category`);

--
-- Klíče pro tabulku `pzshop_product_attributes`
--
ALTER TABLE `pzshop_product_attributes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `product_id` (`product_id`);

--
-- Klíče pro tabulku `pzshop_product_messages`
--
ALTER TABLE `pzshop_product_messages`
  ADD PRIMARY KEY (`id`),
  ADD KEY `product_id` (`product_id`);

--
-- Klíče pro tabulku `pzshop_product_photos`
--
ALTER TABLE `pzshop_product_photos`
  ADD PRIMARY KEY (`id`),
  ADD KEY `product_id` (`product_id`);

--
-- Klíče pro tabulku `pzshop_users`
--
ALTER TABLE `pzshop_users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT pro tabulky
--

--
-- AUTO_INCREMENT pro tabulku `pzshop_categories`
--
ALTER TABLE `pzshop_categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=49;
--
-- AUTO_INCREMENT pro tabulku `pzshop_products`
--
ALTER TABLE `pzshop_products`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=51;
--
-- AUTO_INCREMENT pro tabulku `pzshop_product_attributes`
--
ALTER TABLE `pzshop_product_attributes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=29;
--
-- AUTO_INCREMENT pro tabulku `pzshop_product_messages`
--
ALTER TABLE `pzshop_product_messages`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT pro tabulku `pzshop_product_photos`
--
ALTER TABLE `pzshop_product_photos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pro tabulku `pzshop_users`
--
ALTER TABLE `pzshop_users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- Omezení pro exportované tabulky
--

--
-- Omezení pro tabulku `pzshop_products`
--
ALTER TABLE `pzshop_products`
  ADD CONSTRAINT `Fkey1` FOREIGN KEY (`category`) REFERENCES `pzshop_categories` (`id`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Omezení pro tabulku `pzshop_product_attributes`
--
ALTER TABLE `pzshop_product_attributes`
  ADD CONSTRAINT `Fkey2` FOREIGN KEY (`product_id`) REFERENCES `pzshop_products` (`id`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Omezení pro tabulku `pzshop_product_messages`
--
ALTER TABLE `pzshop_product_messages`
  ADD CONSTRAINT `Fkey3` FOREIGN KEY (`product_id`) REFERENCES `pzshop_products` (`id`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Omezení pro tabulku `pzshop_product_photos`
--
ALTER TABLE `pzshop_product_photos`
  ADD CONSTRAINT `Fkey_products_photos` FOREIGN KEY (`product_id`) REFERENCES `pzshop_products` (`id`) ON DELETE SET NULL ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
